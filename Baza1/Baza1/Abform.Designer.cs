﻿namespace Baza1
{
    partial class Abform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this._Name = new System.Windows.Forms.TextBox();
            this.sName = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.Address = new System.Windows.Forms.TextBox();
            this.Comment = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Birthday = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(235, 242);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Ок";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(95, 242);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Отменить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // _Name
            // 
            this._Name.Location = new System.Drawing.Point(51, 12);
            this._Name.Name = "_Name";
            this._Name.Size = new System.Drawing.Size(152, 20);
            this._Name.TabIndex = 2;
            // 
            // sName
            // 
            this.sName.Location = new System.Drawing.Point(51, 38);
            this.sName.Name = "sName";
            this.sName.Size = new System.Drawing.Size(152, 20);
            this.sName.TabIndex = 3;
            // 
            // tName
            // 
            this.tName.Location = new System.Drawing.Point(51, 65);
            this.tName.Name = "tName";
            this.tName.Size = new System.Drawing.Size(152, 20);
            this.tName.TabIndex = 4;
            // 
            // Address
            // 
            this.Address.Location = new System.Drawing.Point(51, 92);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(152, 20);
            this.Address.TabIndex = 5;
            // 
            // Comment
            // 
            this.Comment.Location = new System.Drawing.Point(51, 125);
            this.Comment.Name = "Comment";
            this.Comment.Size = new System.Drawing.Size(152, 20);
            this.Comment.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(264, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Имя";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(264, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Фамилия";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(266, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Отчество";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(266, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Адрес";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(264, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Комментарий";
            // 
            // Birthday
            // 
            this.Birthday.Location = new System.Drawing.Point(51, 152);
            this.Birthday.Name = "Birthday";
            this.Birthday.Size = new System.Drawing.Size(152, 20);
            this.Birthday.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(269, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Дата рождения";
            // 
            // Abform
            // 
            this.ClientSize = new System.Drawing.Size(450, 277);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Birthday);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Comment);
            this.Controls.Add(this.Address);
            this.Controls.Add(this.tName);
            this.Controls.Add(this.sName);
            this.Controls.Add(this._Name);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Abform";
            this.Text = "Абонент";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.TextBox _Name;
        public System.Windows.Forms.TextBox sName;
        public System.Windows.Forms.TextBox tName;
        public System.Windows.Forms.TextBox Address;
        public System.Windows.Forms.TextBox Comment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox Birthday;


    }
}