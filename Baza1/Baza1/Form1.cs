﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Baza1
{
    public partial class Form1 : Form
    {
        private string ConnectionString = global::Baza1.Properties.Settings.Default.Database2ConnectionString;
        public Form1()
        {
            InitializeComponent();
        }


        private void search_TextChanged(object sender, EventArgs e)
        {
            UpdateAbonentDGV();
            UpdateSprav();
        }

        private void AppdateSQL()
        {
            SqlConnection sqlc = new SqlConnection();
            sqlc.ConnectionString = ConnectionString;
            sqlc.Open();

        }


        private string connectionstring = global::Baza1.Properties.Settings.Default.Database2ConnectionString;
        private string abonentTableName = "Chugrina_abonent";
        private string contactTableName = "Chugrina_contact";
        private string providerTableName = "Chugrina_provider";
        void UpdateAbonentDGV()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var state = connection.State;

            var request = "SELECT * FROM [" + abonentTableName + "] WHERE surname + ' ' + name +' '+ patronymic LIKE '%" + search_fio.Text + "%'";//добавить фильтр по фио и номеру телефона

            var adapter = new SqlDataAdapter(request, connectionstring);
            var abonentTable = new DataTable();
            adapter.Fill(abonentTable);
            tabl_abonent.DataSource = abonentTable;

            tabl_abonent.Columns["id"].Visible = false;
            tabl_abonent.Columns["name"].HeaderText = "Имя";
            tabl_abonent.Columns["patronymic"].HeaderText = "Отчество";
            tabl_abonent.Columns["surname "].HeaderText = "Фамилия";
            tabl_abonent.Columns["address"].HeaderText = "Адрес";
            tabl_abonent.Columns["birthdate"].HeaderText = "День рождения";
            tabl_abonent.Columns["comment"].HeaderText = "Комментарий";
        }
        void UpdateContactDGV()
        {
            var request = "SELECT * From [" + contactTableName + "] WHERE phone + ' ' + type LIKE '%" + search_phone.Text + "%'";

            var adapter = new SqlDataAdapter(request, connectionstring);
            var contactTable = new DataTable();
            adapter.Fill(contactTable);
            tabl_contact.DataSource = contactTable;
            tabl_contact.Columns["id"].Visible = false;
            tabl_contact.Columns["provider_id"].Visible = false;
            tabl_contact.Columns["phone"].HeaderText = "Телефон";
            tabl_contact.Columns["type"].HeaderText = "Тип номера";

        }
        void UpdateProvDGV()
        {
            var request = "SELECT * From Chugrina_provider";
            var adapter = new SqlDataAdapter(request, connectionstring);
            var provTable = new DataTable();
            adapter.Fill(provTable);
            tabl_provider.DataSource = provTable;
            tabl_provider.Columns["id"].Visible = false;
            tabl_provider.Columns["name"].HeaderText = "Провайдер";
            tabl_provider.Columns["score"].HeaderText = "Оценка";
        }

        void UpdateSprav()
        {

            var request = @"SELECT * FROM Chugrina_abonent RIGHT JOIN Chugrina_abonent_hos_cont ON Chugrina_abonent.Id=Chugrina_abonent_hos_cont.abonent_id LEFT JOIN Chugrina_contact ON Chugrina_contact.Id=Chugrina_abonent_hos_cont.contact_id LEFT JOIN Chugrina_provider ON Chugrina_contact.provider_id=Chugrina_provider.Id WHERE 1=1";

            if (search_phone.Text != "")
            {
                request += @" AND Chugrina_contact.phone LIKE '%" + search_phone.Text + "%'";

            }

            if (search_fio.Text != "")
            {
                request += @" AND Chugrina_abonent.surname+' '+Chugrina_abonent.patronymic+' '+Chugrina_abonent.name  LIKE+'%" + search_fio.Text + "%'";
            }
            
            var adapter = new SqlDataAdapter(request, connectionstring);
            var listtable = new DataTable();
            adapter.Fill(listtable);
            tabl_spravochnik.DataSource = listtable;
            tabl_spravochnik.Columns["Id"].Visible = false;
            tabl_spravochnik.Columns["abonent_id"].Visible = false;
            tabl_spravochnik.Columns["contact_id"].Visible = false;
            tabl_spravochnik.Columns["Id1"].Visible = false;
            tabl_spravochnik.Columns["Id2"].Visible = false;
            tabl_spravochnik.Columns["name"].HeaderText = "Имя";
            tabl_spravochnik.Columns["surname "].HeaderText = "Фамилия";
            tabl_spravochnik.Columns["patronymic"].HeaderText = "Отчество";
            tabl_spravochnik.Columns["birthdate"].HeaderText = "Дата Рождения";
            tabl_spravochnik.Columns["comment"].Visible = false;
            tabl_spravochnik.Columns["address"].HeaderText = "Адрес";
            tabl_spravochnik.Columns["phone"].HeaderText = "Номер";
            tabl_spravochnik.Columns["type"].HeaderText = "Тип";
            tabl_spravochnik.Columns["provider_id"].Visible = false;
            tabl_spravochnik.Columns["name1"].HeaderText = "Провайдер";
            tabl_spravochnik.Columns["score"].Visible = false;

        }
        private void Form1_Load_1(object sender, EventArgs e)
        {

            // TODO: This line of code loads data into the 'database2DataSet.Chugrina_abonent' table. You can move, or remove it, as needed.
            this.chugrina_abonentTableAdapter.Fill(this.database2DataSet.Chugrina_abonent);
            UpdateAbonentDGV();
            UpdateContactDGV();
            UpdateProvDGV();
            UpdateSprav();

        }



        private void search_phone_TextChanged(object sender, EventArgs e)
        {
            UpdateContactDGV();
            UpdateSprav();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            var form = new Abform();

            /* {
                 var request = "SELECT *FROM Chugrina_provider";
                 var adapter = new SqlDataAdapter(request, connectionstring);
                 var orgtable = new DataTable();
                 adapter.Fill(orgtable);
                 var dict = new Dictionary<int, string>();
                 foreach (DataRow row in orgtable.Rows)
                 {
                     dict.Add((int)row["id"], row["name"].ToString());
                 }
                 form.Organizationdata = dict;
             }*/
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var fname = form._Name.Text;
                var sname = form.sName.Text;
                var tname = form.tName.Text;
                var comment = form.Comment.Text;
                var addres = form.Address.Text;
                var birthday = form.Birthday.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var reqiest = "INSERT INTO Chugrina_abonent (name,surname, patronymic, birthdate, comment, address) VALUES ('" + fname + "' ,'" + sname + "' ,'" + tname + "' ,'" + birthday + "' ,'" + comment + "' ,'" + addres + "')";
                var command = new SqlCommand(reqiest, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateAbonentDGV();
            }
        }

        private void Change_Click(object sender, EventArgs e)
        {
            var rov = tabl_abonent.SelectedRows.Count > 0 ? tabl_abonent.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Строчку выберите", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new Abform();

            form._Name.Text = rov.Cells["name"].Value.ToString();
            form.sName.Text = rov.Cells["surname "].Value.ToString();
            form.tName.Text = rov.Cells["patronymic"].Value.ToString();
            form.Comment.Text = rov.Cells["comment"].Value.ToString();
            form.Address.Text = rov.Cells["address"].Value.ToString();
            form.Birthday.Text = rov.Cells["birthdate"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var fname = form._Name.Text;
                var sname = form.sName.Text;
                var tname = form.tName.Text;
                var comment = form.Comment.Text;
                var addres = form.Address.Text;
                var birthday = Convert.ToDateTime(form.Birthday.Text).ToString("yyyy-MM-dd HH:mm:ss.fff");
                var connection = new SqlConnection(ConnectionString);
                var id = rov.Cells["Id"].Value.ToString();
                //var orgId = form.OrgId;
                connection.Open();
                var reqiest = "UPDATE Chugrina_abonent SET name ='" + fname + "', surname ='" + sname + "', patronymic='"
                    + tname + "', comment='" + comment + "', address='" + addres + "',birthdate = '" + birthday + "' WHERE Id=" + id + ";";
                var command = new SqlCommand(reqiest, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateAbonentDGV();
            }


        }

        private void Delete_Click(object sender, EventArgs e)
        {
            var rov = tabl_abonent.SelectedRows.Count > 0 ? tabl_abonent.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Строчку выбери", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var connection = new SqlConnection(ConnectionString);
            var id = rov.Cells["Id"].Value.ToString();
            connection.Open();
            var reqiest = "DELETE FROM Chugrina_abonent_hos_cont  WHERE abonent_id=" + id + "DELETE FROM Chugrina_abonent WHERE Id=" + id;
            var command = new SqlCommand(reqiest, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateAbonentDGV();
        }

        private void Searchphone_Click(object sender, EventArgs e)
        {
            UpdateContactDGV();
        }

        private void Add_pr_Click(object sender, EventArgs e)
        {
            var form = new Prform();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.name_prov.Text;
                var score = form.score_prov.Text;

                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var reqiest = "INSERT INTO [" + providerTableName + "] (name,score) VALUES ('" + name + "' ,'" + score + "')";
                var command = new SqlCommand(reqiest, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateProvDGV();
            }
        }

        private void Edit_pr_Click(object sender, EventArgs e)
        {
            var rov = tabl_provider.SelectedRows.Count > 0 ? tabl_provider.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Строчку выберите", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new Prform();

            form.name_prov.Text = rov.Cells["name"].Value.ToString();
            form.score_prov.Text = rov.Cells["score"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var namepr = form.name_prov.Text;
                var scorepr = form.score_prov.Text;

                var connection = new SqlConnection(ConnectionString);
                var id = rov.Cells["Id"].Value.ToString();
                //var orgId = form.OrgId;
                connection.Open();
                var reqiest = "UPDATE [" + providerTableName + "] SET name ='" + namepr + "', score ='" + scorepr;
                var command = new SqlCommand(reqiest, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateProvDGV();
            }
        }

        private void Delete_pr_Click(object sender, EventArgs e)
        {
            var rov = tabl_provider.SelectedRows.Count > 0 ? tabl_provider.SelectedRows[0] : null;
            if (rov == null)
            {
                MessageBox.Show("Строчку выбери", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var connection = new SqlConnection(ConnectionString);
            var id = rov.Cells["Id"].Value.ToString();
            connection.Open();
            var reqiest = "DELETE FROM Chugrina_provider WHERE Id=" + id + ";";
            var command = new SqlCommand(reqiest, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateProvDGV();
            UpdateContactDGV();
            UpdateSprav();
            UpdateAbonentDGV();
        }

        private void Add_cont_Click(object sender, EventArgs e)
        {
            var form = new Contform();
            {
                var getReq = "SELECT *FROM Chugrina_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow row in providerTbl.Rows)
                {
                    dict.Add((int)row["Id"], row["name"].ToString());
                }
                form.ProviderData = dict;
            }

            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.ConForm_Phone_Number.Text;
                var type = form.ConForm_Type.Text;
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Chugrina_contact (phone, type, provider_id) VALUES ('" + phone + "', '" + type + "', '" + provider_id.ToString() + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateContactDGV();
            }
        }

        private void Edit_cont_Click(object sender, EventArgs e)
        {
            var row = tabl_contact.SelectedRows.Count > 0 ? tabl_contact.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new Contform();
            form.ConForm_Phone_Number.Text = row.Cells["phone"].Value.ToString();
            form.ConForm_Type.Text = row.Cells["type"].Value.ToString();
            {
                var getReq = "SELECT *FROM Chugrina_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow dbrow in providerTbl.Rows)
                {
                    dict.Add((int)dbrow["Id"], dbrow["name"].ToString());
                }
                form.ProviderData = dict;
            }
            form.ProviderID = (int)row.Cells["provider_id"].Value;
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.ConForm_Phone_Number.Text;
                var type = form.ConForm_Type.Text;
                var id = row.Cells["Id"].Value.ToString();
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Chugrina_contact SET phone = '" + phone + "', type = '" + type + "', provider_id=" + provider_id.ToString() +
                     "WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                UpdateContactDGV();
            }
        }

        private void Delete_cont_Click(object sender, EventArgs e)
        {
            var row = tabl_contact.SelectedRows.Count > 0 ? tabl_contact.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Chugrina_contact WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateContactDGV();
        }
        //cpravochnick
        private void Add_phone_to_fio_Click(object sender, EventArgs e)
        {
            var form = new Phone_to_fio();

            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + abonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }
                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT Id, phone FROM" + "[" + contactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }
                form.ContactData = dict;
            }

            if (form.ShowDialog() == DialogResult.OK)
            {
                var conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "INSERT INTO Chugrina_abonent_hos_cont" + "(abonent_id, contact_id)" + " VALUES " + "('" + form.AbonentID + "', '" + form.ContactID + "')";
                var com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();

                UpdateSprav();
            }
        }

        private void Edit_phone_to_fio_Click(object sender, EventArgs e)
        {
            var row2 = tabl_spravochnik.SelectedRows.Count > 0 ? tabl_spravochnik.SelectedRows[0] : null;
            if (row2 == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new Phone_to_fio();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + abonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }


                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT Id, phone FROM" + "[" + contactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }


                form.ContactData = dict;
            }
            DataGridViewSelectedRowCollection Row = tabl_spravochnik.SelectedRows;

            var mas = tabl_spravochnik.SelectedRows;
            var Condidat = mas[0].Cells["Id"].FormattedValue.ToString();
            var Condidat2 = mas[0].Cells["Id1"].FormattedValue.ToString();

            var conn = new SqlConnection(ConnectionString);
            conn.Open();
            string str = "";
            var req = "SELECT surname FROM Chugrina_abonent WHERE Id = " + Condidat + "";
            var com = new SqlCommand(req, conn);
            var last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT name FROM Chugrina_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT patronymic FROM Chugrina_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last;
            form.FIO_PhDirecForm_ComBox.Text = str;

            req = "SELECT phone FROM Chugrina_contact WHERE Id = " + Condidat2 + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            form.Phone_PhDirecForm_ComBox.Text = last.ToString();

            conn.Close();

            if (form.ShowDialog() == DialogResult.OK)
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "UPDATE Chugrina_abonent_hos_cont " + " SET contact_id='" + form.ContactID +
                    "', " + "abonent_id ='" + form.AbonentID + "' WHERE contact_id=" + Condidat2 +
                    " AND abonent_id=" + Condidat;
                com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();


                UpdateSprav();

            }
        }

        private void Delete_phone_to_fio_Click(object sender, EventArgs e)
        {
            var row = tabl_spravochnik.SelectedRows.Count > 0 ? tabl_spravochnik.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var num1 = row.Cells["Id"].Value.ToString();
            var num2 = row.Cells["Id1"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Chugrina_abonent_hos_cont WHERE contact_id = '" + num2 +
                "' AND abonent_id='" + num1 + "'";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            UpdateSprav();
        }

    }
}
        