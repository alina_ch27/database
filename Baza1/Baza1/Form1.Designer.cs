﻿namespace Baza1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.search_fio = new System.Windows.Forms.TextBox();
            this.Contactmain = new System.Windows.Forms.TabControl();
            this.abonent_page = new System.Windows.Forms.TabPage();
            this.tabl_abonent = new System.Windows.Forms.DataGridView();
            this.Add_ab = new System.Windows.Forms.Button();
            this.Delete_ab = new System.Windows.Forms.Button();
            this.Edit_ab = new System.Windows.Forms.Button();
            this.contact_page = new System.Windows.Forms.TabPage();
            this.Delete_cont = new System.Windows.Forms.Button();
            this.Edit_cont = new System.Windows.Forms.Button();
            this.Add_cont = new System.Windows.Forms.Button();
            this.tabl_contact = new System.Windows.Forms.DataGridView();
            this.provider_page = new System.Windows.Forms.TabPage();
            this.Delete_pr = new System.Windows.Forms.Button();
            this.Edit_pr = new System.Windows.Forms.Button();
            this.Add_pr = new System.Windows.Forms.Button();
            this.tabl_provider = new System.Windows.Forms.DataGridView();
            this.spravochnik_page = new System.Windows.Forms.TabPage();
            this.tabl_spravochnik = new System.Windows.Forms.DataGridView();
            this.search_phone = new System.Windows.Forms.TextBox();
            this.database2DataSet = new Baza1.Database2DataSet();
            this.database2DataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.chugrinaabonentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.chugrina_abonentTableAdapter = new Baza1.Database2DataSetTableAdapters.Chugrina_abonentTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Add_phone_to_fio = new System.Windows.Forms.Button();
            this.Edit_phone_to_fio = new System.Windows.Forms.Button();
            this.Delete_phone_to_fio = new System.Windows.Forms.Button();
            this.Contactmain.SuspendLayout();
            this.abonent_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_abonent)).BeginInit();
            this.contact_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_contact)).BeginInit();
            this.provider_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_provider)).BeginInit();
            this.spravochnik_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_spravochnik)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.database2DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.database2DataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chugrinaabonentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // search_fio
            // 
            this.search_fio.Location = new System.Drawing.Point(838, 573);
            this.search_fio.Name = "search_fio";
            this.search_fio.Size = new System.Drawing.Size(100, 20);
            this.search_fio.TabIndex = 0;
            this.search_fio.UseWaitCursor = true;
            this.search_fio.TextChanged += new System.EventHandler(this.search_TextChanged);
            // 
            // Contactmain
            // 
            this.Contactmain.Controls.Add(this.abonent_page);
            this.Contactmain.Controls.Add(this.contact_page);
            this.Contactmain.Controls.Add(this.provider_page);
            this.Contactmain.Controls.Add(this.spravochnik_page);
            this.Contactmain.Location = new System.Drawing.Point(0, 0);
            this.Contactmain.Name = "Contactmain";
            this.Contactmain.SelectedIndex = 0;
            this.Contactmain.Size = new System.Drawing.Size(922, 492);
            this.Contactmain.TabIndex = 1;
            // 
            // abonent_page
            // 
            this.abonent_page.Controls.Add(this.tabl_abonent);
            this.abonent_page.Controls.Add(this.Add_ab);
            this.abonent_page.Controls.Add(this.Delete_ab);
            this.abonent_page.Controls.Add(this.Edit_ab);
            this.abonent_page.Location = new System.Drawing.Point(4, 22);
            this.abonent_page.Name = "abonent_page";
            this.abonent_page.Padding = new System.Windows.Forms.Padding(3);
            this.abonent_page.Size = new System.Drawing.Size(914, 466);
            this.abonent_page.TabIndex = 0;
            this.abonent_page.Text = "Абоненты";
            this.abonent_page.UseVisualStyleBackColor = true;
            // 
            // tabl_abonent
            // 
            this.tabl_abonent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_abonent.Location = new System.Drawing.Point(3, 0);
            this.tabl_abonent.Name = "tabl_abonent";
            this.tabl_abonent.Size = new System.Drawing.Size(904, 404);
            this.tabl_abonent.TabIndex = 0;
            // 
            // Add_ab
            // 
            this.Add_ab.Location = new System.Drawing.Point(41, 427);
            this.Add_ab.Name = "Add_ab";
            this.Add_ab.Size = new System.Drawing.Size(129, 23);
            this.Add_ab.TabIndex = 5;
            this.Add_ab.Text = "Добавить абонента";
            this.Add_ab.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Add_ab.UseVisualStyleBackColor = true;
            this.Add_ab.Click += new System.EventHandler(this.Add_Click);
            // 
            // Delete_ab
            // 
            this.Delete_ab.Location = new System.Drawing.Point(715, 427);
            this.Delete_ab.Name = "Delete_ab";
            this.Delete_ab.Size = new System.Drawing.Size(121, 23);
            this.Delete_ab.TabIndex = 6;
            this.Delete_ab.Text = "Удалить абонента";
            this.Delete_ab.UseVisualStyleBackColor = true;
            this.Delete_ab.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Edit_ab
            // 
            this.Edit_ab.Location = new System.Drawing.Point(383, 427);
            this.Edit_ab.Name = "Edit_ab";
            this.Edit_ab.Size = new System.Drawing.Size(118, 23);
            this.Edit_ab.TabIndex = 7;
            this.Edit_ab.Text = "Изменить абонента";
            this.Edit_ab.UseVisualStyleBackColor = true;
            this.Edit_ab.Click += new System.EventHandler(this.Change_Click);
            // 
            // contact_page
            // 
            this.contact_page.Controls.Add(this.Delete_cont);
            this.contact_page.Controls.Add(this.Edit_cont);
            this.contact_page.Controls.Add(this.Add_cont);
            this.contact_page.Controls.Add(this.tabl_contact);
            this.contact_page.Location = new System.Drawing.Point(4, 22);
            this.contact_page.Name = "contact_page";
            this.contact_page.Padding = new System.Windows.Forms.Padding(3);
            this.contact_page.Size = new System.Drawing.Size(914, 466);
            this.contact_page.TabIndex = 1;
            this.contact_page.Text = "Контакты";
            this.contact_page.UseVisualStyleBackColor = true;
            // 
            // Delete_cont
            // 
            this.Delete_cont.Location = new System.Drawing.Point(642, 435);
            this.Delete_cont.Name = "Delete_cont";
            this.Delete_cont.Size = new System.Drawing.Size(128, 23);
            this.Delete_cont.TabIndex = 3;
            this.Delete_cont.Text = "Удалить контакт";
            this.Delete_cont.UseVisualStyleBackColor = true;
            this.Delete_cont.Click += new System.EventHandler(this.Delete_cont_Click);
            // 
            // Edit_cont
            // 
            this.Edit_cont.Location = new System.Drawing.Point(353, 435);
            this.Edit_cont.Name = "Edit_cont";
            this.Edit_cont.Size = new System.Drawing.Size(153, 23);
            this.Edit_cont.TabIndex = 2;
            this.Edit_cont.Text = "Изменить контакт";
            this.Edit_cont.UseVisualStyleBackColor = true;
            this.Edit_cont.Click += new System.EventHandler(this.Edit_cont_Click);
            // 
            // Add_cont
            // 
            this.Add_cont.Location = new System.Drawing.Point(70, 435);
            this.Add_cont.Name = "Add_cont";
            this.Add_cont.Size = new System.Drawing.Size(162, 23);
            this.Add_cont.TabIndex = 1;
            this.Add_cont.Text = "Добавить контакт";
            this.Add_cont.UseVisualStyleBackColor = true;
            this.Add_cont.Click += new System.EventHandler(this.Add_cont_Click);
            // 
            // tabl_contact
            // 
            this.tabl_contact.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_contact.Location = new System.Drawing.Point(0, 6);
            this.tabl_contact.Name = "tabl_contact";
            this.tabl_contact.Size = new System.Drawing.Size(907, 409);
            this.tabl_contact.TabIndex = 0;
            // 
            // provider_page
            // 
            this.provider_page.Controls.Add(this.Delete_pr);
            this.provider_page.Controls.Add(this.Edit_pr);
            this.provider_page.Controls.Add(this.Add_pr);
            this.provider_page.Controls.Add(this.tabl_provider);
            this.provider_page.Location = new System.Drawing.Point(4, 22);
            this.provider_page.Name = "provider_page";
            this.provider_page.Padding = new System.Windows.Forms.Padding(3);
            this.provider_page.Size = new System.Drawing.Size(914, 466);
            this.provider_page.TabIndex = 2;
            this.provider_page.Text = "Провайдеры";
            this.provider_page.UseVisualStyleBackColor = true;
            // 
            // Delete_pr
            // 
            this.Delete_pr.Location = new System.Drawing.Point(685, 426);
            this.Delete_pr.Name = "Delete_pr";
            this.Delete_pr.Size = new System.Drawing.Size(164, 23);
            this.Delete_pr.TabIndex = 3;
            this.Delete_pr.Text = "Удалить провайдера";
            this.Delete_pr.UseVisualStyleBackColor = true;
            this.Delete_pr.Click += new System.EventHandler(this.Delete_pr_Click);
            // 
            // Edit_pr
            // 
            this.Edit_pr.Location = new System.Drawing.Point(341, 427);
            this.Edit_pr.Name = "Edit_pr";
            this.Edit_pr.Size = new System.Drawing.Size(200, 23);
            this.Edit_pr.TabIndex = 2;
            this.Edit_pr.Text = "Изменить провайдера";
            this.Edit_pr.UseVisualStyleBackColor = true;
            this.Edit_pr.Click += new System.EventHandler(this.Edit_pr_Click);
            // 
            // Add_pr
            // 
            this.Add_pr.Location = new System.Drawing.Point(51, 426);
            this.Add_pr.Name = "Add_pr";
            this.Add_pr.Size = new System.Drawing.Size(146, 23);
            this.Add_pr.TabIndex = 1;
            this.Add_pr.Text = "Добавить провайдера";
            this.Add_pr.UseVisualStyleBackColor = true;
            this.Add_pr.Click += new System.EventHandler(this.Add_pr_Click);
            // 
            // tabl_provider
            // 
            this.tabl_provider.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_provider.Location = new System.Drawing.Point(0, 0);
            this.tabl_provider.Name = "tabl_provider";
            this.tabl_provider.Size = new System.Drawing.Size(907, 408);
            this.tabl_provider.TabIndex = 0;
            // 
            // spravochnik_page
            // 
            this.spravochnik_page.Controls.Add(this.Delete_phone_to_fio);
            this.spravochnik_page.Controls.Add(this.Edit_phone_to_fio);
            this.spravochnik_page.Controls.Add(this.Add_phone_to_fio);
            this.spravochnik_page.Controls.Add(this.tabl_spravochnik);
            this.spravochnik_page.Location = new System.Drawing.Point(4, 22);
            this.spravochnik_page.Name = "spravochnik_page";
            this.spravochnik_page.Padding = new System.Windows.Forms.Padding(3);
            this.spravochnik_page.Size = new System.Drawing.Size(914, 466);
            this.spravochnik_page.TabIndex = 3;
            this.spravochnik_page.Text = "Тел.справочник";
            this.spravochnik_page.UseVisualStyleBackColor = true;
            // 
            // tabl_spravochnik
            // 
            this.tabl_spravochnik.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_spravochnik.Location = new System.Drawing.Point(-4, 0);
            this.tabl_spravochnik.Name = "tabl_spravochnik";
            this.tabl_spravochnik.Size = new System.Drawing.Size(918, 400);
            this.tabl_spravochnik.TabIndex = 0;
            // 
            // search_phone
            // 
            this.search_phone.Location = new System.Drawing.Point(838, 533);
            this.search_phone.Name = "search_phone";
            this.search_phone.Size = new System.Drawing.Size(100, 20);
            this.search_phone.TabIndex = 3;
            this.search_phone.TextChanged += new System.EventHandler(this.search_phone_TextChanged);
            // 
            // database2DataSet
            // 
            this.database2DataSet.DataSetName = "Database2DataSet";
            this.database2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // database2DataSetBindingSource
            // 
            this.database2DataSetBindingSource.DataSource = this.database2DataSet;
            this.database2DataSetBindingSource.Position = 0;
            // 
            // chugrinaabonentBindingSource
            // 
            this.chugrinaabonentBindingSource.DataMember = "Chugrina_abonent";
            this.chugrinaabonentBindingSource.DataSource = this.database2DataSetBindingSource;
            // 
            // chugrina_abonentTableAdapter
            // 
            this.chugrina_abonentTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(730, 536);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Поиск по номеру";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(730, 573);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Поиск по имени";
            // 
            // Add_phone_to_fio
            // 
            this.Add_phone_to_fio.Location = new System.Drawing.Point(88, 420);
            this.Add_phone_to_fio.Name = "Add_phone_to_fio";
            this.Add_phone_to_fio.Size = new System.Drawing.Size(98, 23);
            this.Add_phone_to_fio.TabIndex = 1;
            this.Add_phone_to_fio.Text = "Добавить";
            this.Add_phone_to_fio.UseVisualStyleBackColor = true;
            this.Add_phone_to_fio.Click += new System.EventHandler(this.Add_phone_to_fio_Click);
            // 
            // Edit_phone_to_fio
            // 
            this.Edit_phone_to_fio.Location = new System.Drawing.Point(326, 419);
            this.Edit_phone_to_fio.Name = "Edit_phone_to_fio";
            this.Edit_phone_to_fio.Size = new System.Drawing.Size(110, 23);
            this.Edit_phone_to_fio.TabIndex = 2;
            this.Edit_phone_to_fio.Text = "Изменить";
            this.Edit_phone_to_fio.UseVisualStyleBackColor = true;
            this.Edit_phone_to_fio.Click += new System.EventHandler(this.Edit_phone_to_fio_Click);
            // 
            // Delete_phone_to_fio
            // 
            this.Delete_phone_to_fio.Location = new System.Drawing.Point(607, 419);
            this.Delete_phone_to_fio.Name = "Delete_phone_to_fio";
            this.Delete_phone_to_fio.Size = new System.Drawing.Size(75, 23);
            this.Delete_phone_to_fio.TabIndex = 3;
            this.Delete_phone_to_fio.Text = "Удалить";
            this.Delete_phone_to_fio.UseVisualStyleBackColor = true;
            this.Delete_phone_to_fio.Click += new System.EventHandler(this.Delete_phone_to_fio_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 605);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.search_phone);
            this.Controls.Add(this.Contactmain);
            this.Controls.Add(this.search_fio);
            this.Name = "Form1";
            this.Text = "Телефонный справочник";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.Contactmain.ResumeLayout(false);
            this.abonent_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_abonent)).EndInit();
            this.contact_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_contact)).EndInit();
            this.provider_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_provider)).EndInit();
            this.spravochnik_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_spravochnik)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.database2DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.database2DataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chugrinaabonentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox search_fio;
        private System.Windows.Forms.TabControl Contactmain;
        private System.Windows.Forms.DataGridView tabl_abonent;
        private System.Windows.Forms.TabPage contact_page;
        private System.Windows.Forms.DataGridView tabl_contact;
        private System.Windows.Forms.TabPage provider_page;
        private System.Windows.Forms.DataGridView tabl_provider;
        private System.Windows.Forms.TabPage spravochnik_page;
        private System.Windows.Forms.DataGridView tabl_spravochnik;
        private System.Windows.Forms.TabPage abonent_page;
        private System.Windows.Forms.TextBox search_phone;
        private System.Windows.Forms.BindingSource database2DataSetBindingSource;
        private Database2DataSet database2DataSet;
        private System.Windows.Forms.BindingSource chugrinaabonentBindingSource;
        private Database2DataSetTableAdapters.Chugrina_abonentTableAdapter chugrina_abonentTableAdapter;
        private System.Windows.Forms.Button Add_ab;
        private System.Windows.Forms.Button Delete_ab;
        private System.Windows.Forms.Button Edit_ab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Delete_cont;
        private System.Windows.Forms.Button Edit_cont;
        private System.Windows.Forms.Button Add_cont;
        private System.Windows.Forms.Button Delete_pr;
        private System.Windows.Forms.Button Edit_pr;
        private System.Windows.Forms.Button Add_pr;
        private System.Windows.Forms.Button Delete_phone_to_fio;
        private System.Windows.Forms.Button Edit_phone_to_fio;
        private System.Windows.Forms.Button Add_phone_to_fio;
    }
}

