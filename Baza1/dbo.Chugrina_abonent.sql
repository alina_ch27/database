﻿CREATE TABLE [dbo].[Chugrina_abonent] (
    [Id]         INT        IDENTITY (1, 1) NOT NULL,
    [name]       NCHAR (50) NOT NULL,
    [surname ]   NCHAR (50) NOT NULL,
    [patronymic] NCHAR (50) NOT NULL,
    [birthdate]  DATETIME   NOT NULL,
    [comment]    NTEXT      NULL,
    [address]    TEXT       NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

