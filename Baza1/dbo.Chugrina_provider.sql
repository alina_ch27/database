﻿CREATE TABLE [dbo].[Chugrina_provider] (
    [Id]    INT        IDENTITY (1, 1) NOT NULL,
    [name1]  NCHAR (50) NOT NULL,
    [score] FLOAT (53) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

