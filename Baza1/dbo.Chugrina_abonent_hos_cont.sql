﻿CREATE TABLE [dbo].[Chugrina_abonent_hos_cont] (
    [abonent_id] INT NOT NULL,
    [contact_id] INT NOT NULL,
    CONSTRAINT [FK_Chugrina_abonent_hos_cont_Chugrina_abonent] FOREIGN KEY ([abonent_id]) REFERENCES [dbo].[Chugrina_abonent] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Chugrina_abonent_hos_cont_Chugrina_contact] FOREIGN KEY ([contact_id]) REFERENCES [dbo].[Chugrina_contact] ([Id]) ON DELETE CASCADE
);

