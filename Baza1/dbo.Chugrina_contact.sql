﻿CREATE TABLE [dbo].[Chugrina_contact] (
    [Id]          INT        IDENTITY (1, 1) NOT NULL,
    [phone]       NCHAR (11) NOT NULL,
    [type]        NCHAR (20) NOT NULL,
    [provider_id] INT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Chugrina_contact_Chugrina_provider] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[Chugrina_provider] ([Id]) ON DELETE CASCADE
);

